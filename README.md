# XDLinux Issues

[提交新的Issue](https://gitlab.com/XDOSC/WIFI/issues/new)

## 镜像站403

校园网流量10元3GB，我们无法负担高昂的流量费用，因此只对校内提供服务。

如果您是校内用户（使用了校园网或翼迅）并且无法访问，
希望您能将IP（可通过 https://linux.xidian.edu.cn/ip 或系统命令查看）发给我们，以便我们将您所在的IP段加入白名单。

白名单都是社区内成员以及同学们自发贡献的，因此可能存在疏漏。

## 申请新镜像

你可以通过提交 Issue 的方式建议 XDLinux 为某个开源项目进行镜像。

请在申请内容中写明：
* 项目名称
* 项目简介
* 上游源地址，或镜像方法
* 所需架构、版本
* 尽量详细的镜像信息（例如镜像大小）

## 服务故障

软件源同步错误具有偶然性，请等待下次同步之后再试试。

请在申请内容中写明：
* 服务名称
* 详细出错信息
* 错误重现步骤

## 服务列表

* [Mirrors](https://linux.xidian.edu.cn/mirrors)
* [Git](https://linux.xidian.edu.cn/git)
* [BBS](https://linux.xidian.edu.cn/bbs)
* [Wiki](https://linux.xidian.edu.cn/wiki)
* [Minecraft](https://linux.xidian.edu.cn/wiki/guide/minecraft)

[服务器状态](https://linux.xidian.edu.cn/status)

# 西电开源社区指南

加入开源社区之后，不要潜水和旁观，多参与活动和讨论，与成员分享知识，相互学习，让大家认识你。

## 活动

1. Software Freedom Day 软件自由日
   * 每年9月第三个星期六（今年由于学校举办互联网+，推迟到9月23日）
2. Hacking Thursday 每周小聚（不一定在周四）
   * 围绕一个主题，协作、分享、提问、交流
   * 计划在图书馆研讨室进行，不点名、不强制
3. 自发的主题演讲
   * 不定期地分享一些东西，一般会选择在每周小聚进行
4. 约吃约喝约玩
   * 任何人都可以组织，可以发布到邮件列表或者论坛

## 交流

1. 即时通讯：平时聊天+水的地方，用机器人实现互通

   * QQ：`226198461`，大家应该都很熟悉了
   * Telegram：`@xdosc`，跨平台，里面有很多好用的功能，但是需要科学上网

2. 论坛：弥补即时通讯工具讨论的分类、时效性等不足

   * `bbs.xdlinux.info`，通过 RSS 订阅新主题到 Telegram

3. 邮件列表：和论坛作用相似，更加稳定可靠，建议订阅以接收通知

   * `https://groups.google.com/forum/#!forum/xidian_linux`
   * 如何订阅邮件列表
     * 发送任意内容邮件至 `xidian_linux+subscribe@googlegroups.com`，并回复确认邮件
   * 如何创建一个主题
     * 发送邮件（标题+正文）至 `xidian_linux@googlegroups.com`
   * 如何回复一个主题
     * 回复相应邮件即可

## 服务

* 主页：`xdlinux.info`， `linux.xidian.edu.cn`
* 镜像站：`mirrors.xdlinux.info`
* Git 代码托管：`git.xdlinux.info` 
* Wiki：`wiki.xdlinux.info`
* Minecraft Server：详见 Wiki 页面
